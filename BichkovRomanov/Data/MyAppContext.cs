﻿using BichkovRomanov.Models;
using Microsoft.EntityFrameworkCore;

namespace BichkovRomanov.Data
{
    public class MyAppContext : DbContext
    {
        public DbSet<User> Users { get; set; }
        public DbSet<Letter> Letters { get; set; }


        public MyAppContext(DbContextOptions<MyAppContext> options)
            : base(options)
        {
            Database.EnsureCreated();   // создаем базу данных при первом обращении
        }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            // configures one-to-many relationship
            modelBuilder.Entity<Letter>()
                .HasOne<User>(l => l.Sender)
                .WithMany(l => l.SendedLetters);

            modelBuilder.Entity<Letter>()
                .HasOne(l => l.Reciever)
                .WithMany(l => l.RecievedLetters);

        }
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder
                .UseLazyLoadingProxies();
        }
        public DbSet<BichkovRomanov.Models.UserCountLettersInfoApiModel> UserCountLettersInfoApiModel { get; set; }
    }
}
