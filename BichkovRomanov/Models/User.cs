﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BichkovRomanov.Models
{
    public class User
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }
        public string FatherName { get; set; }
        public DateTime DayOfBirth { get; set; }
        public virtual List<Letter> RecievedLetters { get; set; }
        public virtual List<Letter> SendedLetters { get; set; }
    }
}
