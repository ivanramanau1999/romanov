﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BichkovRomanov.Models
{
    public enum SortState
    {
        TopicAsc,
        TopicDesc
    }
}
