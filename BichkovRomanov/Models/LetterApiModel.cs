﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BichkovRomanov.Models
{
    public class LetterApiModel
    {
        public string LetterTopic { get; set; }
        public string LetterText { get; set; }
        public string RecieverFullName { get; set; }
    }
}
