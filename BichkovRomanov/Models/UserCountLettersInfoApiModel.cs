﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace BichkovRomanov.Models
{
    [NotMapped]
    public class UserCountLettersInfoApiModel : User
    {
        public int SendLettersCount { get; set; }
        public int RecieveLetterCount { get; set; }
    }
}
