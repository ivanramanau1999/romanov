﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BichkovRomanov.Models
{
    public class Letter
    {
        public int Id { get; set; }
        public string LetterTopic { get; set; }
        public string LetterText { get; set; }
        public DateTime SendDate { get; set; }
        public virtual User Sender { get; set; }
        public virtual User Reciever { get; set; }
    }
}
