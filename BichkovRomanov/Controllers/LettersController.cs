﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using BichkovRomanov.Data;
using BichkovRomanov.Models;

namespace BichkovRomanov.Controllers
{
    public class LettersController : Controller
    {
        private readonly MyAppContext _context;

        public LettersController(MyAppContext context)
        {
            _context = context;
        }

        // GET: Letters
        public async Task<IActionResult> Index(SortState sortOrder = SortState.TopicAsc)
        {
            IQueryable<Letter> letters = _context.Letters;

            ViewData["TopicSort"] = sortOrder == SortState.TopicAsc ? SortState.TopicDesc : SortState.TopicAsc;

            letters = sortOrder switch
            {
                SortState.TopicDesc => letters.OrderByDescending(s => s.LetterTopic),
                _ => letters.OrderBy(s => s.LetterTopic),
            };

            return View(await letters.AsNoTracking().ToListAsync());
        }

        // GET: Letters/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var letter = await _context.Letters
                .FirstOrDefaultAsync(m => m.Id == id);
            if (letter == null)
            {
                return NotFound();
            }

            return View(letter);
        }

        // GET: Letters/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: Letters/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,LetterTopic,LetterText,SendDate, RecieverFullName")] LetterApiModel letter)
        {
            if (ModelState.IsValid)
            {
                string[] inputName = letter.RecieverFullName.Split(" ");
                Letter letterToSave = new Letter
                {
                    Id = 0,
                    LetterTopic = letter.LetterTopic,
                    LetterText = letter.LetterText,
                    SendDate = DateTime.Now,
                    /*Reciever = _context.Query<User>().Where(u => u.Name == inputName[0] && u.Surname == inputName[1]).FirstOrDefault()*/
                };
                _context.Add(letterToSave);
                _context.SaveChanges();
                return RedirectToAction(nameof(Index));
            }
            return View(letter);
        }

        // GET: Letters/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var letter = await _context.Letters.FindAsync(id);
            if (letter == null)
            {
                return NotFound();
            }
            return View(letter);
        }

        // POST: Letters/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,LetterTopic,LetterText,SendDate")] Letter letter)
        {
            if (id != letter.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(letter);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!LetterExists(letter.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(letter);
        }

        // GET: Letters/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var letter = await _context.Letters
                .FirstOrDefaultAsync(m => m.Id == id);
            if (letter == null)
            {
                return NotFound();
            }

            return View(letter);
        }

        // POST: Letters/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var letter = await _context.Letters.FindAsync(id);
            _context.Letters.Remove(letter);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool LetterExists(int id)
        {
            return _context.Letters.Any(e => e.Id == id);
        }
    }
}
