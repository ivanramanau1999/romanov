﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using BichkovRomanov.Data;
using BichkovRomanov.Models;

namespace BichkovRomanov.Controllers
{
    public class UsersController : Controller
    {
        private readonly MyAppContext _context;

        public UsersController(MyAppContext context)
        {
            _context = context;
        }

        // GET: Users
        public async Task<IActionResult> Index()
        {
            return View(await _context.Users.ToListAsync());
        }

        // GET: Users/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var user = await _context.Users
                .FirstOrDefaultAsync(m => m.Id == id);
            if (user == null)
            {
                return NotFound();
            }

            return View(user);
        }

        // GET: Users/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: Users/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,Name,Surname,FatherName,DayOfBirth")] User user)
        {
            if (ModelState.IsValid)
            {
                _context.Add(user);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(user);
        }

        // GET: Users/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var user = await _context.Users.FindAsync(id);
            if (user == null)
            {
                return NotFound();
            }
            return View(user);
        }

        // POST: Users/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,Name,Surname,FatherName,DayOfBirth")] User user)
        {
            if (id != user.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(user);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!UserExists(user.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(user);
        }

        // GET: Users/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var user = await _context.Users
                .FirstOrDefaultAsync(m => m.Id == id);
            if (user == null)
            {
                return NotFound();
            }

            return View(user);
        }

        // POST: Users/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var user = await _context.Users.FindAsync(id);
            _context.Users.Remove(user);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool UserExists(int id)
        {
            return _context.Users.Any(e => e.Id == id);
        }

        public IActionResult GetUserWithLowerLetter()
        {
            Letter let = _context.Letters.OrderBy(l =>l.LetterText.Length).First();
            User user = _context.Find<Letter>(let.Id).Sender;
            return View(user);
        }

        public IActionResult GetUsersWithCountLettersInfo()
        {
            List<UserCountLettersInfoApiModel> users = _context.Users.Select(u => new UserCountLettersInfoApiModel
            {
                Id = u.Id,
                Name = u.Name,
                Surname = u.Surname,
                FatherName = u.FatherName,
                DayOfBirth = u.DayOfBirth,
                RecieveLetterCount = u.RecievedLetters.Count(l => l.Reciever.Id == u.Id),
                SendLettersCount = u.SendedLetters.Count(l => l.Sender.Id == u.Id)
            }).ToList();


            return View(users);
        }

        public IActionResult GetUserWithNoTopicLetter()
        {
            List<User> users = _context.Users.Where(u => u.SendedLetters.All(l => l.LetterTopic == null)).ToList();
            return View(users);
        }

        public IActionResult GetUserWithTopicLetter()
        {
            List<User> users = _context.Users.Where(u => u.SendedLetters.All(l => l.LetterTopic != null)).ToList();
            return View(users);
        }

        [HttpGet]
        public IActionResult ForwardSend()
        {
            return View();
        }
        [HttpPost]
        public IActionResult ForwardSend(int senderId, string topic)
        {
            List<Letter> list = new List<Letter>();
            User user = _context.Find<User>(senderId);
            foreach(var letter in user.SendedLetters)
            {
                list.Add(new Letter
                {
                    LetterTopic = topic,
                    LetterText = "text",
                    SendDate = DateTime.Now,
                    Sender = user,
                    Reciever = letter.Reciever
                });
            }
            foreach(var l in list)
            {
                _context.Add(l);
            }
            _context.SaveChanges();
            return Ok();
        }
    }
}
